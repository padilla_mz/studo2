
// ************  Superslides

$(function() {
      var $slides = $('#slides');


      Hammer($slides[0]).on("swipeleft", function(e) {
        $slides.data('superslides').animate('next');
      });

      Hammer($slides[0]).on("swiperight", function(e) {
        $slides.data('superslides').animate('prev');
      });

      $slides.superslides({
        hashchange: true,
        play: 4500,
        animation: 'fade',
      });
    });


/***************** Waypoints ******************/

$(document).ready(function() {


  $('.wp1').waypoint(function() {
    $('.wp1').addClass('animated fadeInLeft');
    
  });

  $('.wp2').waypoint(function() {
    $('.wp2 h2').addClass('animated fadeInUp');
    $('.wp2 p').addClass('animated zoomIn');
    
  });

  $('.wp3').waypoint(function() {
    $('.wp3 h2').addClass('animated fadeInLeft');
    $('.wp3 p').addClass('animated zoomIn');
  });

  $('.wp4').waypoint(function() {
    $('.wp4 h2').addClass('animated fadeInUp');
    $('.wp4 p').addClass('animated zoomIn');
  });
  
  $('.wp5').waypoint(function(direction) {
    if (direction == 'down'){
      $('.wp5').addClass('fixed');
    }
    else if(direction == 'up'){
      $('.wp5').removeClass('fixed');
    }
  });

});